FROM angular2-base:1.0

RUN git clone https://gitlab.com/adriguez/angular-app-hackaton.git

WORKDIR /src/angular-app-hackaton/app
RUN mkdir node_modules
RUN chmod -R 0777 node_modules

RUN npm install

CMD [ "npm", "start" ]
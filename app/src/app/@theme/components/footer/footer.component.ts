import { Component } from '@angular/core';

@Component({
  selector: 'ngx-footer',
  styleUrls: ['./footer.component.scss'],
  template: `
    <span class="created-by">Hackaton DAR 2018 - Barcelona</span>
  `,
})
export class FooterComponent {
}

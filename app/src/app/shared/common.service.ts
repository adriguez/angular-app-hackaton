import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs/Rx';
import { Subject }    from 'rxjs/Subject';
import { Http, Response, Headers, URLSearchParams, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class CommonService {

  readLogs:BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  temperatureService:BehaviorSubject<string> = new BehaviorSubject<string>("");
  optionService:BehaviorSubject<string> = new BehaviorSubject<string>("");
  nameSplit:BehaviorSubject<string> = new BehaviorSubject<string>("");
  roomSelectedUser:BehaviorSubject<string> = new BehaviorSubject<string>("");

  constructor (
    private http: Http,
    private requestOptions: RequestOptions
  ) {}

  baseURL = "http://main:3001/?action=getSensorData&simulate=1&originSC=[ORIGIN_SC]";
  baseURLLogs = "http://logging:4000/";

  // Envío de Request al módulo principal para orquestar la llamada al Sensor y posteriormente a la IA.
  sendRequest(idSC: string) {
    let URLRequest = this.baseURL.replace("[ORIGIN_SC]", idSC);

    console.log('Enviando petición a... ' + URLRequest);
    return this.http.get(URLRequest)
    .map(this.extractData);
  }

  cleanLogs(){

    let bodyPostRequest = {
      'cleanLog': 'true'
    };

    // Header para modificar cabecera OPTIONS por POST enviada desde Angular hasta el Logging Server.
    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    let options = new RequestOptions({ headers: headers });

    return this.http.post(this.baseURLLogs, bodyPostRequest, options)
    .map(this.extractData);

  }

  getLogs(){

    let bodyPostRequest = {
      'action': 'getLogs',
      't': new Date().getTime()
    };

    // Header para modificar cabecera OPTIONS por POST enviada desde Angular hasta el Logging Server.
    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    let options = new RequestOptions({ headers: headers });

    return this.http.post(this.baseURLLogs, bodyPostRequest, options)
    .map(this.extractData);

  }

  private extractData(res: Response) {
    return res.json();
  }

}
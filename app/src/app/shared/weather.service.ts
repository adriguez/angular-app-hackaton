import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class DataService {
  
  location:string = "";

  constructor(private http: Http) {}

  private usersUrl = 'http://api.openweathermap.org/data/2.5/forecast?q=Barcelona,ES&appid=fc74c7ac18780794eb97752a77823574';

  getData() {    
    return this.http.get(this.usersUrl)
    .map(this.extractData)
  }

  private extractData(res: Response) {
    return res.json();
    //let body = res.json();
    //return body.data || { };
  }
  
  private handleError (error: any) {
    // In a real world app, we might use a remote logging infrastructure
    // We'd also dig deeper into the error to get a better message
    let errMsg = (error.message) ? error.message :
    error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
    return Observable.throw(errMsg);
  }
}
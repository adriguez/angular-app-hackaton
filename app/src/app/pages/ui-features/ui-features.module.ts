import { NgModule } from '@angular/core';

import { ThemeModule } from '../../@theme/theme.module';
import { UiFeaturesRoutingModule } from './ui-features-routing.module';
import { UiFeaturesComponent } from './ui-features.component';
import { ModalsComponent } from './modals/modals.component';
import { ModalComponent } from './modals/modal/modal.component';
import { TabsComponent, Tab1Component, Tab2Component } from './tabs/tabs.component';

const components = [
  UiFeaturesComponent,
  ModalsComponent,
  ModalComponent,
  TabsComponent,
  Tab1Component,
  Tab2Component
];

@NgModule({
  imports: [
    ThemeModule,
    UiFeaturesRoutingModule,
  ],
  declarations: [
    ...components,
  ],
  entryComponents: [
    ModalComponent,
  ],
})
export class UiFeaturesModule { }

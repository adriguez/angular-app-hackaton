import { Component, Input } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CommonService } from '../../../shared/common.service';
import {  BlockUI, NgBlockUI } from 'ng-block-ui';
import { ModalComponent } from '../../ui-features/modals/modal/modal.component';
import { ToasterService, ToasterConfig, Toast, BodyOutputType } from 'angular2-toaster';

import 'style-loader!angular2-toaster/toaster.css';

@Component({
  selector: 'ngx-status-card',
  styleUrls: ['./status-card.component.scss'],
  template: `
    <nb-card class="handHover" (click)="testSplit()" [ngClass]="{'off': roomSelectedUser!=id && roomSelectedUser!=''}">
      <div class="icon-container">
        <div class="icon {{ type }}">
          <ng-content></ng-content>
        </div>
      </div>
      <div class="details">
        <div class="title">{{title}}</div>
        <div class="status">{{ on ? 'TESTING...' : 'CLICK PARA TEST' }}</div>
      </div>
    </nb-card>
  `
})
export class StatusCardComponent {

  @BlockUI() blockUI: NgBlockUI;

  @Input() title: string;
  @Input() type: string;
  @Input() on = false;
  @Input() id: string;
  
  readLogs:boolean = false;
  temperatureService:string = "";
  optionService:string = "";
  nameSplit:string = "--";
  roomSelectedUser:string = "";

  testingSplit: string;
  config: ToasterConfig;

  constructor(private _commonService: CommonService, private modalService: NgbModal, private toasterService: ToasterService){

        this._commonService.readLogs.subscribe(readLogsData => {
            this.readLogs = readLogsData
        });

        this._commonService.temperatureService.subscribe(temperatureServiceData => {
            this.temperatureService = temperatureServiceData
        });

        this._commonService.roomSelectedUser.subscribe(roomSelectedUser => {
            this.roomSelectedUser = roomSelectedUser
        });
  }

  private showToast(type: string, title: string, body: string) {
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 5000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this.toasterService.popAsync(toast);
  }

  changeValueLogs = (value) => {
    this.readLogs = value;
    this._commonService.readLogs.next(this.readLogs);
  };

  changeTemperature = (value) => {
    this.temperatureService = value;
    this._commonService.temperatureService.next(this.temperatureService);
  };

  changeOption = (value) => {
    this.optionService = value;
    this._commonService.optionService.next(this.optionService);
  };

  changeNameSplit = (value) => {
    this.nameSplit = "Respuesta IA para " + value;
    this._commonService.nameSplit.next(this.nameSplit);
  };

  testSplit(){
    this.changeValueLogs(true);
    this.changeNameSplit(this.title);
    this.blockUI.start('Realizando TEST. Espere por favor...');
    this.testingSplit = this.id;
    this._commonService.sendRequest(this.id).subscribe(
      x => {    
        this.changeValueLogs(false);

        var temperature = x.response.responseIA.temperature.label.replace("ºC","");
        if(temperature === "0") temperature = "";
        
        this.changeTemperature(temperature);
        this.changeOption(x.response.responseIA.action.id);
        this.showToast('info', 'Respuesta de la IA Recibida!', '');

        this.blockUI.stop();
      },
      x => {
        this.changeValueLogs(false);
        this.blockUI.stop();
        const activeModal = this.modalService.open(ModalComponent, { size: 'lg', container: 'nb-layout' });
        activeModal.componentInstance.modalHeader = '¡Error realizando petición!';
        activeModal.componentInstance.modalContent = `Ha ocurrido un error realizando la petición. Mira la consola, seguro que te ayuda.... ;)`;
      },
      () => {
        this.changeValueLogs(false);
        this.blockUI.stop();
      }
    );
  }

}

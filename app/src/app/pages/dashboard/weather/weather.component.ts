import {Component} from '@angular/core';
import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {HttpModule} from '@angular/http';
import {Observable} from 'rxjs/Observable';

import 'rxjs/add/operator/map';

@Injectable()
export class WeatherService {
  
  constructor(private http: Http) {}
  private weatherUrl = 'http://api.apixu.com/v1/forecast.json?key=e5719ce574b6468b80e195354180703&q=Barcelona, Spain&days=5';

  getData() {
    return this.http.get(this.weatherUrl)
    .map(this.extractData)
  }

  private extractData(res: Response) {
    return res.json();
  }
  
  private handleError (error: any) {
    // In a real world app, we might use a remote logging infrastructure
    // We'd also dig deeper into the error to get a better message
    let errMsg = (error.message) ? error.message :
    error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
    return Observable.throw(errMsg);
  }
}

@Component({
  selector: 'ngx-weather',
  styleUrls: ['./weather.component.scss'],
  providers: [WeatherService],
  templateUrl: './weather.component.html',
})
export class WeatherComponent {

  forecastHtml = [];
  currentWeather = {};

  constructor(weatherService: WeatherService) {

    function getDayWeekString(day, type="short"){
      var dayStr = "";
      switch(day){
        case 1: if(type !== "short") dayStr = "Lunes"; else dayStr = "LUN"; break;
        case 2: if(type !== "short") dayStr = "Martes"; else dayStr = "MAR"; break;
        case 3: if(type !== "short") dayStr = "Miércoles"; else dayStr = "MIE"; break;
        case 4: if(type !== "short") dayStr = "Jueves"; else dayStr = "JUE"; break;
        case 5: if(type !== "short") dayStr = "Viernes"; else dayStr = "VIE"; break;
        case 6: if(type !== "short") dayStr = "Sábado"; else dayStr = "SAB"; break;
        case 0: if(type !== "short") dayStr = "Domingo"; else dayStr = "DOM"; break;
      }
      return dayStr;
    }

    function getMonthString(monthInt){
      var monthStr = "";
      switch(monthInt){
        case 1: monthStr = "Enero"; break;
        case 2: monthStr = "Febrero"; break;
        case 3: monthStr = "Marzo"; break;
        case 4: monthStr = "Abril"; break;
        case 5: monthStr = "Mayo"; break;
        case 6: monthStr = "Junio"; break;
        case 7: monthStr = "Julio"; break;
        case 8: monthStr = "Agosto"; break;
        case 9: monthStr = "Septiembre"; break;
        case 10: monthStr = "Octubre"; break;
        case 11: monthStr = "Noviembre"; break;
        case 12: monthStr = "Diciembre"; break;
      }
      return monthStr;
    }

    weatherService.getData().subscribe(
      x => {
      	let forecast = [];
        let currentWeather = {};

        var hoy = new Date();
        var hoyString  = getDayWeekString(hoy.getDay(), "long") + " " + hoy.getDate() + " " + getMonthString(hoy.getMonth());

        if(x.current !== undefined){
          currentWeather = {
            'date': hoyString,
            'temp': x.current.temp_c,
            'tempMax': x.forecast.forecastday[0]['day']['maxtemp_c'],
            'tempMin': x.forecast.forecastday[0]['day']['mintemp_c'],
            'viento': x.current.vis_km,
            'humedad': x.current.humidity,
            'icon': x.current.condition.icon.replace("64x64","128x128")
          }
        }

        if(x.forecast !== undefined){
          for(var i=1;i<x.forecast.forecastday.length;i++){
            var dateTimestamp = x.forecast.forecastday[i]['date_epoch'];
            dateTimestamp = new Date(dateTimestamp * 1000);
            var fecha = getDayWeekString(dateTimestamp.getDay());
            var forecastObject = {
              'date': fecha,
              'tempAvg': x.forecast.forecastday[i].day.avgtemp_c,
              'icon': x.forecast.forecastday[i].day.condition.icon.replace("64x64","64x64")
            }
            forecast.push(forecastObject);
          }
        }

        this.forecastHtml = forecast;
        this.currentWeather = currentWeather;

        console.log("Weather data received: ",x);
      },
      x => {
        console.log("Error weather data: ",x);
      },
      () => {
        console.log("Completed");
      }
    );
  }
  
}
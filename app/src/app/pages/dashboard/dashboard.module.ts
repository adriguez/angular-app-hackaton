import { NgModule } from '@angular/core';

import { NgxEchartsModule } from 'ngx-echarts';

import { ToasterModule } from 'angular2-toaster';
import { ThemeModule } from '../../@theme/theme.module';
import { DashboardComponent } from './dashboard.component';
import { StatusCardComponent } from './status-card/status-card.component';
import { RoomsComponent } from './rooms/rooms.component';
import { RoomSelectorComponent } from './rooms/room-selector/room-selector.component';
import { TemperatureComponent } from './temperature/temperature.component';
import { TemperatureDraggerComponent } from './temperature/temperature-dragger/temperature-dragger.component';
import { CodeConsoleComponent } from './codeconsole/codeconsole.component';
import { WeatherComponent } from './weather/weather.component';
import { CodemirrorModule } from 'ng2-codemirror';
import { BlockUIModule } from 'ng-block-ui';
import { ModalComponent } from '../ui-features/modals/modal/modal.component';

@NgModule({
  imports: [
    ThemeModule,
    NgxEchartsModule,
    CodemirrorModule,
    ToasterModule,
    BlockUIModule
  ],
  declarations: [
    DashboardComponent,
    StatusCardComponent,
    TemperatureDraggerComponent,
    RoomSelectorComponent,
    TemperatureComponent,
    RoomsComponent,
    CodeConsoleComponent,
    WeatherComponent,
    ModalComponent
  ],
   entryComponents: [
    ModalComponent
  ],
})
export class DashboardModule { }

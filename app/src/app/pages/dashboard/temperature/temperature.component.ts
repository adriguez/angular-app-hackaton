import { Component, OnDestroy } from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { CommonService } from '../../../shared/common.service';

@Component({
  selector: 'ngx-temperature',
  styleUrls: ['./temperature.component.scss'],
  templateUrl: './temperature.component.html'
})
export class TemperatureComponent implements OnDestroy {

  temperature = "";
  temperatureOff = true;
  temperatureMode = 'heat';
  optionService = "";
  nameSplit = "";

  colors: any;
  themeSubscription: any;

  constructor(private theme: NbThemeService, private _commonService: CommonService) {
    this.themeSubscription = this.theme.getJsTheme().subscribe(config => {
      this.colors = config.variables;
    });

    this._commonService.temperatureService.subscribe(temperatureService => {
      this.temperature = temperatureService;
      if(this.temperature !=='') this.temperatureOff = false;
      else this.temperatureOff = true;
    });

    this._commonService.optionService.subscribe(optionService => {
      this.optionService = optionService;
    });

    this._commonService.nameSplit.subscribe(nameSplit => {
      this.nameSplit = nameSplit;
    });

  }

  ngOnDestroy() {
    this.themeSubscription.unsubscribe();
  }
}

import { Component, OnDestroy } from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { CommonService } from '../../../shared/common.service';

@Component({
  selector: 'ngx-codeconsole',
  styleUrls: ['./codeconsole.component.scss'],
  templateUrl: './codeconsole.component.html'
})
export class CodeConsoleComponent implements OnDestroy {

  readLogs:boolean;
  data: Array<any>;

  currentTheme: string;
  themeSubscription: any;
    
  config = { readOnly:true, lineNumbers: true, mode: 'javascript', theme: 'mdn-like' };
  code = ``;

  constructor(private themeService: NbThemeService, private _commonService: CommonService) {
    this.themeSubscription = this.themeService.getJsTheme().subscribe(theme => {
      this.currentTheme = theme.name;
    });
    
    this.getLogs();
    // setInterval to read logs each 3 sec.
    setInterval(() => {
      this.getLogs();
    }, 3000);

    this._commonService.readLogs.subscribe(readLogsData => {
      this.readLogs = readLogsData
    });

  }

  // Function to clean logs
  cleanLogs(){
    this._commonService.cleanLogs().subscribe(
      x => { 

        this.code = ``;
        this.getLogs();
        
      },
      x => {
        console.log("Error limpiando logs: ",x);
      },
      () => {}
    );
  }

  // Function to get logs
  getLogs(){
    this._commonService.getLogs().subscribe(
      x => { 

        this.code = ``;    

        for(var i=0;i<x.length;i++){
          this.code += "[" + x[i].date + "]" + " [" + x[i].type + "] " + "[" + x[i].module + "] " + x[i].logString + '\n';
        }
        
      },
      x => {
        console.log("Error data: ",x);
      },
      () => {}
    );
  }

  ngOnDestroy() {
    this.themeSubscription.unsubscribe();
  }
}

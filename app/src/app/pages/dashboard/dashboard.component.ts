import { Component } from '@angular/core';
import { ToasterService, ToasterConfig, Toast, BodyOutputType } from 'angular2-toaster';

@Component({
  selector: 'ngx-dashboard',
  styleUrls: ['./dashboard.component.scss'],
  templateUrl: './dashboard.component.html',
})
export class DashboardComponent {

	config: ToasterConfig;

	constructor(private toasterService: ToasterService){
	    this.config = new ToasterConfig({
	      positionClass: 'toast-top-center',
	      timeout: 5000,
	      newestOnTop: true,
	      tapToDismiss: true,
	      preventDuplicates: true,
	      animation: 'slideup',
	      limit: 5
	    });	
	}



}
